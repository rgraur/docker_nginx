# Nginx Docker image #

### Base image ###
* official [Ubuntu image](https://registry.hub.docker.com/_/ubuntu/)

### Installed packages ###
* nginx

### Specifications ###
* forwarded port: 80

### Run ###
* build image: `docker build -t nginx .`
* create container: `docker run -p 80:80 -v /host/web/path:/var/www:rw --name nginx -d nginx`
* to link with a database container (e.g. container name mysql) use: `docker run -p 80:80 -v /host/web/path:/var/www:rw --link mysql:db --name nginx -d nginx`

